#include "Cloud.h"

Cloud::Cloud() {
}

Cloud::~Cloud() {
}


Cloud::Cloud(int resource, int x, int y) {
	this->resource = resource;
	this->x = x*WIDTH_CLOUD;
	this->y = y*HEIGHT_CLOUD;
}

void Cloud::draw(SDLInterface& graphic) {
	graphic.drawTexture(resource, 0, 0, WIDTH_CLOUD, HEIGHT_CLOUD, x, y, WIDTH_CLOUD, HEIGHT_CLOUD); 
}

void Cloud::update() {
	this->x += 1;
	if (x >= SCREEN_WITDH) {
		x = -WIDTH_CLOUD;
	}
}
