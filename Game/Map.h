#pragma once
#include "SDLInterface.h"
#include "Element.h"
#include "Cloud.h"
#include "Wall.h"
#include "Void.h"
#include "Final.h"
#include "Start.h"
#include "FileReader.h"
#include <vector>

#define XMAX 20
#define YMAX 13

#define DIMENSION_BACKGROUND 0
#define DIMENSION_PLATFORMS 1

class Map {
	Element* map[20][13][2];

public:
	Map();
	~Map();

	void drawMap(SDLInterface& graphic);
	void update();
	int getTexture(int x, int y);
	Element& getCell(int x, int y);
};

