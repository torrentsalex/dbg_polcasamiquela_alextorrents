#include "Collision.h"



Collision::Collision() {
}

Collision::~Collision() {
}

// Check 2 objects if have intersect between
bool Collision::GetColision(Character& c, Element& e) {
	// x Axis
	if ((c.getX() >= e.getX() && c.getX() <= (e.getX() + e.getWidth())) ||
		((c.getX() + c.getWidth()) >= e.getX() && (c.getX() + c.getWidth()) <= (e.getX() + e.getWidth()))) {
		// y Axis;
		if ((c.getY() >= e.getY() && c.getY() <= (e.getY() + e.getHeight())) ||
			((c.getY() + c.getHeight()) >= e.getY() && (c.getY() + c.getHeight()) <= (e.getY() + e.getHeight()))) {
			//The sprites appear to overlap.
			return true;
		}
	}
	return false;
}