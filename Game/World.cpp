#include "World.h"
#include "stdio.h"
#include <algorithm> 

World::World() {
}

World::World(World& world) {
	pj = world.pj;
	map = world.map;
	listOfEnemies = world.listOfEnemies;
	listOfPeople = world.listOfPeople;
}

World::World(int screenWidth, int screenHeight) {
	this->map = Map();

	Element* enemy;
	vector<vector<int>> vEnemies = FileReader::LoadArrayFromFile("../sharedResources/files/enemies.txt");
	for (int i = 0; i < vEnemies.size(); i++) {
		if (i % 2 == 0) {
			enemy = new EnemyHoritzontal(HELICOPTER_LEFT, vEnemies.at(i).at(0), vEnemies.at(i).at(1));
		} else {
			enemy = new EnemyVertical(HELICOPTER, vEnemies.at(i).at(0), vEnemies.at(i).at(1));
		}
		listOfEnemies.add(enemy);
	}
}

bool World::newGame() {
	if (!FileReader::CheckExistFile("../sharedResources/files/player.txt") || 
			!FileReader::CheckExistFile("../sharedResources/files/gunters.txt")) {
		return false;
	}
	// Load the positions of the enemies and gunters (penguins) with the file
	vector<vector<int>> vCharacter = FileReader::LoadArrayFromFile("../sharedResources/files/player.txt");
	vector<vector<int>> vGunter = FileReader::LoadArrayFromFile("../sharedResources/files/gunters.txt");

	// Ask to user for the name
	char name[max_char];
	cout << "Your name: " << endl;
	cin >> name;

	// init character
	this->pj = Character(OVNI_IMAGE, vCharacter.at(0).at(0), vCharacter.at(0).at(1), name);
	// init people
	Element *people;
	for (int i = 0; i < vGunter.size();i++) {
		people = new Person(GUNTER, vGunter.at(i).at(0), vGunter.at(i).at(1));
		listOfPeople.add(people);
	}
	return true;
}

bool World::loadGame() {
	if (!FileReader::CheckExistFile("../sharedResources/files/store/player.txt") ||
			!FileReader::CheckExistFile("../sharedResources/files/store/punctuation.bin") ||
			!FileReader::CheckExistFile("../sharedResources/files/store/gunters.txt")) {
		return false;
	}
	// Load the positions of the enemies and gunters (penguins) with the file
	vector<vector<int>> vCharacter = FileReader::LoadArrayFromFile("../sharedResources/files/store/player.txt");
	vector<vector<int>> vGunter = FileReader::LoadArrayFromFile("../sharedResources/files/store/gunters.txt");
	vector<Player> vPunctation = FileReader::LoadPlayersBinary("../sharedResources/files/store/punctuation.bin");
	// init character
	this->pj = Character(OVNI_IMAGE, vCharacter.at(0).at(0), vCharacter.at(0).at(1), vPunctation.at(0).getName());
	this->pj.setPunctuation(vPunctation.at(0).getScore());
	// init people
	Element *people;
	for (int i = 0; i < vGunter.size();i++) {
		people = new Person(GUNTER, vGunter.at(i).at(0), vGunter.at(i).at(1));
		listOfPeople.add(people);
	}
	return true;
}

void World::saveCurrentGame() {
	// gunters
	vector<vector<int>> vVector;
	vector<int> intVector;
	for (int i = 0; i < listOfPeople.getSize(); i++) {
		// Save x and y position
		intVector.push_back(listOfPeople.get(i)->getX() / 70);
		intVector.push_back(listOfPeople.get(i)->getY() / 70);
		// Save the gunter into the gunters vecttor
		vVector.push_back(intVector);
		intVector.clear();
	}
	FileReader::SaveArrayToFile(vVector, "../sharedResources/files/store/gunters.txt");

	vVector.clear();
	intVector.clear();
	// character
	intVector.push_back(pj.getX());
	intVector.push_back(pj.getY());
	vVector.push_back(intVector);
	FileReader::SaveArrayToFile(vVector, "../sharedResources/files/store/player.txt");

	intVector.clear();
	vVector.clear();

	vector<Player> vPlayer;

	vPlayer.push_back(pj.getPlayer());
	FileReader::SavePlayerBinary(vPlayer, "../sharedResources/files/store/punctuation.bin", false);

	saved = true;
}

World::~World() {
}

void World::drawWorld(SDLInterface& graphic) {
	this->map.drawMap(graphic);
	for (int i = 0; i < listOfEnemies.getSize();i++) {
		listOfEnemies.get(i)->draw(graphic);
	}

	//Log::print("drawWorld :", listOfPeople.getSize());
	for (int i = 0; i < listOfPeople.getSize();i++) {
		listOfPeople.get(i)->draw(graphic);
	}
	this->pj.drawCharacter(graphic);
	// Draw the current punctuation
	graphic.drawText("User: "+ pj.getName() + " - Punctuation :" + std::to_string(pj.getPoints()), BLACK, SCREEN_WITDH / 2, 10);
	if (saved) {
		graphic.drawText(stringSave, BLACK, SCREEN_WITDH / 2, SCREEN_HEIGHT / 2);
		savedCounter++;
		if (savedCounter >= 60) { // draw text 60 frames only
			saved = false;
			savedCounter = 0;
		}
	}
}

void World::updateWorld(GameState& gameState) {
	this->pj.update();
	this->map.update();
	for (int i = 0; i < listOfEnemies.getSize();i++) {
		listOfEnemies.get(i)->update();
	}
	// Update the gunters

	for (int i = 0; i < listOfPeople.getSize();i++) {
		listOfPeople.get(i)->update();
	}
	// it's dead and the sprite is over
	if (this->pj.getCurrentFrame() >= 24) { 
		Log::print("FIRE SPRITE OVER");
		// END THE GAME
		gameState = GameState::LOSE;
	}
	if (finnished) {
		calculateRanking();
		gameState = GameState::WIN;
	}

}

void World::physicsWorld() {
	int x = pj.getX();
	int y = pj.getY();
	// Check the collision next cell of Y dimension
	Element nextCell = map.getCell(pj.getX()+CELL_WIDTH, pj.getY());
	
	// This code for collisions with the map is a little creepy, we need to invest more time on it
	// up left corner
	if (map.getTexture(x, y) != NON_MATERIAL &&
			map.getTexture(x, y) != SIGNAL_FINAL &&
			map.getTexture(x, y) != SIGNAL_RIGHT) {
		pj.setDeadProperties(true);
	}
	// up right corner
	if (map.getTexture(x + this->pj.getWidth(), y) != NON_MATERIAL &&
			map.getTexture(x + this->pj.getWidth(), y) != SIGNAL_FINAL &&
			map.getTexture(x + this->pj.getWidth(), y) != SIGNAL_RIGHT) {
		pj.setDeadProperties(true);
	}
	// down left corner
	if (map.getTexture(x, y + this->pj.getHeight()) != NON_MATERIAL &&
			map.getTexture(x, y + this->pj.getHeight()) != SIGNAL_FINAL &&
			map.getTexture(x, y + this->pj.getHeight()) != SIGNAL_RIGHT) {
		
		pj.setDeadProperties(true);
	}
	// down right corner
	if (map.getTexture(x + this->pj.getWidth(), y + this->pj.getHeight()) != NON_MATERIAL &&
			map.getTexture(x + this->pj.getWidth(), y + this->pj.getHeight()) != SIGNAL_FINAL &&
			map.getTexture(x + this->pj.getWidth(), y + this->pj.getHeight()) != SIGNAL_RIGHT) {
		
		pj.setDeadProperties(true);
	}

	// Collision with signal right, 
	if (map.getTexture(pj.getX() + this->pj.getWidth()/2, pj.getY() + this->pj.getHeight()) == SIGNAL_RIGHT) {
		pj.collisioned();
	}

	// Detect if collisioned with the exit sign and finish the game if its true
	if (this->map.getTexture( x + this->pj.getWidth(), y + this->pj.getHeight()) == SIGNAL_FINAL){
		finnished = true;	
	}

	// Check collisions for enemies
	for (int i = 0; i < listOfEnemies.getSize();i++) {
		if (Collision::GetColision(pj, *listOfEnemies.get(i))) {
			this->pj.collisioned();
			this->pj.setDeadProperties(true);
		}
	}

	// Check collisions for penguins
	for (int i = 0; i < listOfPeople.getSize(); i++) {
		if (Collision::GetColision(pj, *listOfPeople.get(i))) {
			pj.plusPoints();
			// remove the gunter touched
			Log::print(" remove, collision");
			listOfPeople.remove(i);
		}
	}
}

void World::executePlayerCommands(SDLInterface& graphic) {
	// Play movement
	if (graphic.isKeyPressed(SDLK_SPACE) || graphic.isKeyPressed(SDLK_w) || graphic.isKeyPressed(SDLK_UP)) {
		this->pj.upPlayer();
	}
	if (graphic.isKeyPressed(SDLK_RIGHT) || graphic.isKeyPressed(SDLK_d)) {
		this->pj.goRight();
	}
	if (graphic.isKeyPressed(SDLK_LEFT) || graphic.isKeyPressed(SDLK_a)) {
		this->pj.goLeft();
	}
	// Save current game
	if (graphic.isKeyPressed(SDLK_e)) {
		this->saveCurrentGame();
	}
}

int World::getFinalPuntuaction() {
	return pj.getPoints();
}


// Private methods
void World::calculateRanking() {
	vector<Player> vPlayers;
	// Loadplayers
	vPlayers = FileReader::LoadPlayersBinary("../sharedResources/files/store/ranking.bin");
	// Add in the last position the current player
	vPlayers.push_back(pj.getPlayer());

	std::sort(vPlayers.begin(), vPlayers.end());

	FileReader::SavePlayerBinary(vPlayers, "../sharedResources/files/store/ranking.bin", false);
}