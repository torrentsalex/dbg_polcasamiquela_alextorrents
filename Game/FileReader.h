#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "Character.h"

using namespace std;

class FileReader {
private:
	FileReader();
	~FileReader();
	static int getRowSize(string file);
	static int getColumnSize(string file);
	static vector<int> split(const string &line);
	ifstream openFile(string file);

public:
	/**
		return array of in
	*/
	static vector<vector<int>> LoadArrayFromFile(std::string fileName);
	static vector<Player> LoadPlayersBinary(std::string fileName);

	static void SaveArrayToFile(vector<vector<int>> vector, std::string fileName);
	static void SavePlayerBinary(vector<Player> p, std::string fileName, bool endFile);

	static bool CheckExistFile(std::string fileName);

};

