#include "Game.h"
#include "FileReader.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	myMenu(screenWidth, screenHeight),
	world(screenWidth, screenHeight) {
}

/**
* Destructor
*/
Game::~Game() {
}

/*
* Game execution
*/
void Game::run() {
		//Prepare the game components
	init();
		//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	srand((unsigned int)time(NULL));
		//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);	
	_graphic.setWindowBackgroundColor(208, 244, 247, 255);
		//Load the sprites associated to the different game elements
	_graphic.loadTexture(SPRITE_FIRE, "../sharedResources/images/characters/explosion.png");
	_graphic.loadTexture(BACKGROUND, "../sharedResources/images/background/ufo_background.jpg");
	_graphic.loadTexture(BUTTON, "../sharedResources/images/objects/button_green.png");
	_graphic.loadTexture(OVNI_IMAGE, "../sharedResources/images/characters/ovni.png");
	_graphic.loadTexture(LOSE_SCREEN, "../sharedResources/images/background/ufo-crash.jpg");
	_graphic.loadTexture(STAR, "../sharedResources/images/objects/star.png");
	_graphic.loadTexture(WIN_SCREEN, "../sharedResources/images/objects/winScreen.png");
	_graphic.loadTexture(LEADERBOARD, "../sharedResources/images/background/lead.png");


	// Load Cell textures
	_graphic.loadTexture(CLOUD, "../sharedResources/images/terrain/cloud1.png");
	_graphic.loadTexture(PLATFORM_TOP_LEFT, "../sharedResources/images/terrain/grassLeft.png");
	_graphic.loadTexture(PLATFORM_TOP_RIGHT, "../sharedResources/images/terrain/grassRight.png");
	_graphic.loadTexture(PLATFORM_CENTER, "../sharedResources/images/terrain/grassCenter.png");
	_graphic.loadTexture(PLATFORM_CENTER_TOP, "../sharedResources/images/terrain/grassMid.png");
	_graphic.loadTexture(WATER, "../sharedResources/images/terrain/liquidWaterTop.png");
	_graphic.loadTexture(SIGNAL_RIGHT, "../sharedResources/images/terrain/signRight.png");
	_graphic.loadTexture(SIGNAL_FINAL, "../sharedResources/images/terrain/signExit.png");
	_graphic.loadTexture(HELICOPTER, "../sharedResources/images/characters/heli.png");
	_graphic.loadTexture(HELICOPTER_LEFT, "../sharedResources/images/characters/helileft.png");
	_graphic.loadTexture(GUNTER, "../sharedResources/images/characters/gunter.png");
		//Set the font style
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::MENU;

	while (_gameState != GameState::EXIT) {		
			//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
			//Execute the player commands 
		executePlayerCommands();
			//Update the game physics
		doPhysics();
			//Render game
		renderGame();			
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {
	
	// detect buttons clicked depending of the game state 
	switch (_gameState) {
		case GameState::MENU:
			if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
				glm::ivec2 d = _graphic.getMouseCoords();
				switch (this->myMenu.onClickButtons(d)) {
					case -1: // Any button pressed
						break;
					case BUTTON_NEW_GAME:
						if (world.newGame()) {
							_gameState = GameState::PLAY;
						} else {
							errorLoadBool = true;
							errorLoad = Error_Load::new_game;
						} 
						break;
					case BUTTON_LOAD_GAME:
						if (world.loadGame()) {
							_gameState = GameState::PLAY;
						} else {
							errorLoadBool = true;
							errorLoad = Error_Load::load_game;
						}
						break;
					case BUTTON_EXIT:
						_gameState = GameState::EXIT;
						break;
					case BUTTON_CREDITS:
						_gameState = GameState::CREDITS;
						break;
					case BUTTON_RANKING:
						_gameState = GameState::RANKING;
						break;
				}
			}
			break;
		case GameState::LOSE:
			if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
				glm::ivec2 d = _graphic.getMouseCoords();
				switch (this->myMenu.onClickButtons(d)) {
				case -1: // Any button pressed
					break;
				case BUTTON_EXIT:
					_gameState = GameState::EXIT;
					break;
				case BUTTON_BACK_TO_MENU:
					_gameState = GameState::MENU;
					break;
				}
			}
		break;
		case GameState::CREDITS:
		case GameState::RANKING:
			if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
				glm::ivec2 d = _graphic.getMouseCoords();
				switch (this->myMenu.onClickButtons(d)) {
					case BUTTON_BACK_TO_MENU:
						_gameState = GameState::MENU;
						break;
				}
			}
			break;
		case GameState::WIN:
			if (_graphic.isKeyPressed(SDL_BUTTON_LEFT)) {
				glm::ivec2 d = _graphic.getMouseCoords();
				switch (this->myMenu.onClickButtons(d)) {
				case BUTTON_EXIT:
					_gameState = GameState::EXIT;
					break;
				case BUTTON_BACK_TO_MENU:
					_gameState = GameState::MENU;
					break;
				}
			}
			break;
		case GameState::PLAY:
			// detect wasd and space 
			this->world.executePlayerCommands(_graphic);
			break;
	}
	
	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}
}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	switch (_gameState) {
		case  GameState::MENU:
			break;
		case GameState::PLAY:
			// Move Elements, and we pass the game state for lose and win
			world.updateWorld(_gameState);
			// Check for collisions
			world.physicsWorld();
			break;
		case GameState::CREDITS:
			break;
	}
}

/**
* Render the game on the screen
*/
void Game::renderGame() {
		//Clear the screen
	_graphic.clearWindow();
		//Draw the screen's content based on the game state
	switch (_gameState) {
		case GameState::MENU:
			myMenu.renderMenu(_graphic);
			break;
		case GameState::PLAY:
			//Draw the game
			world.drawWorld(_graphic);
			break;
		case GameState::CREDITS:
			myMenu.renderCredits(_graphic);
			break;
		case GameState::LOSE:
			myMenu.renderLose(_graphic, world.getFinalPuntuaction());
			break;
		case GameState::WIN:
			myMenu.renderWin(_graphic, world.getFinalPuntuaction());
			break;
		case GameState::RANKING:
			myMenu.renderRanking(_graphic);
			break;
	}

	// Draw feedback error to user while load the game
	if (errorLoadBool) {
		switch (errorLoad) {
		case Error_Load::no_ranking:
			_graphic.drawText("Sorry, any player has finished the game", BLACK, SCREEN_WITDH / 2 - 150, SCREEN_HEIGHT - 250);
			break;
		case Error_Load::new_game:
			_graphic.drawText("uoops, I have not found files of the game", BLACK, SCREEN_WITDH / 2 -150, SCREEN_HEIGHT -250);
			break;
		case Error_Load::load_game:
			_graphic.drawText("You don't have any saved game", BLACK, SCREEN_WITDH / 2 -150, SCREEN_HEIGHT -250);
			break;
		case Error_Load::none:
			break;
		}
		errorLoadCounter++;
		if (errorLoadCounter >=100) {
			errorLoadCounter = 0;
			errorLoadBool = false;
		}
	}
	//Refresh screen
	_graphic.refreshWindow();
}
/*
* Draw the sprite on the screen
* @param sprite is the sprite to be displayed
*/
void Game::drawSprite(Sprite & sprite) {
//	_graphic.drawTexture(sprite.getSpriteId(), SPRITE_DEFAULT_WIDTH*sprite.getCurrentFrame(), 0, SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT
//		, sprite.getXAtWorld(), sprite.getYAtWorld(), SPRITE_DEFAULT_WIDTH, SPRITE_DEFAULT_HEIGHT);
}