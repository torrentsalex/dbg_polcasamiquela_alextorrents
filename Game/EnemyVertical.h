#pragma once
#include "Element.h"


enum class EnemyVerticalState { UP, DOWN };

class EnemyVertical : public Element {
private:
	int originY;
	EnemyVerticalState eState;
public:
	EnemyVertical();
	~EnemyVertical();
	EnemyVertical(int resource, int x, int y);

	void update();
};

