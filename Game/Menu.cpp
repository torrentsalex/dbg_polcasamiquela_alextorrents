#include "Menu.h"

Menu::Menu() {
}

Menu::Menu(int screenWidth, int screenHeight) {
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;
	// Init buttons	
	newGame = Button(BUTTON_NEW_GAME, "New Game", 100, 25, screenWidth / 2 - 100, screenHeight / 2 - 300, BUTTON, BLACK);
	loadGame = Button(BUTTON_LOAD_GAME, "Load Game", 100, 25, screenWidth / 2 - 100, screenHeight / 2 - 200, BUTTON, BLACK);
	ranking = Button(BUTTON_EXIT, "Ranking", 50, 25, screenWidth / 2 - 50, screenHeight / 2 - 100, BUTTON, BLACK);
	creditsButton = Button(BUTTON_CREDITS, "Credits", 50, 25, screenWidth / 2 - 50, screenHeight / 2, BUTTON, BLACK);
	exitButton = Button(BUTTON_EXIT, "Exit", 50, 25, screenWidth / 2 - 50, screenHeight / 2 + 100, BUTTON, BLACK);	
	backToMenuButton = Button(BUTTON_BACK_TO_MENU, "Back", 50, 25, 30, 30, BUTTON, BLACK);
}

Menu::~Menu() {
}


void Menu::renderMenu(SDLInterface& graphic) {
	// menu and credits
	graphic.drawTexture(BACKGROUND, 0, 0, 1920, 1200, 0, 0, SCREEN_WITDH, SCREEN_HEIGHT);
	graphic.drawText(" CONTROLS ", GREEN, this->screenWidth / 2 + 485, this->screenHeight - 200);
	graphic.drawText("ARROWS to move, SPACE to jump, E to save", GREEN, this->screenWidth / 2 +200, this->screenHeight - 150);
	this->newGame.draw(graphic);
	this->loadGame.draw(graphic);
	this->creditsButton.draw(graphic);
	ranking.draw(graphic);
	this->exitButton.draw(graphic);
}

void Menu::renderCredits(SDLInterface& graphic) {
	// draw Made by us
	graphic.drawTexture(BACKGROUND, 0, 0, 1920, 1200, 0, 0, SCREEN_WITDH, SCREEN_HEIGHT);
	graphic.drawText(" Game made By ",  GREEN, this->screenWidth / 2 - 100, this->screenHeight / 4);
	graphic.drawText(" Pol Casamiquela ",  GREEN, this->screenWidth / 2 - 100, this->screenHeight / 4 + 50);
	graphic.drawText(" Alex Torrents ",  GREEN, this->screenWidth / 2 - 100, this->screenHeight / 4 + 100);
	backToMenuButton.draw(graphic);
}

// Draw the lose screen
void Menu::renderLose(SDLInterface& graphic, int punctuation) {
	graphic.drawTexture(LOSE_SCREEN, 0, 0, 1400, 900, 0, 0, 1400, 900);
	this->exitButton.draw(graphic);
	backToMenuButton.draw(graphic);
}

// Draw the win screen
void Menu::renderWin(SDLInterface& graphic, int punctuation) {
	graphic.drawTexture(WIN_SCREEN, 0, 0, 752, 350, SCREEN_WITDH/2 - 800/2, SCREEN_HEIGHT/2 - 400/2, 752, 350);
	graphic.drawText("FINAL PUNCTUATION :" + std::to_string(punctuation), BLACK, SCREEN_WITDH/2 - 135, 470);
	this->exitButton.draw(graphic);
	
	// Calculate the star to show depending of the score
	this->starPunctuation = ((float)(punctuation/10)/ NUM_GUNTER) * 3 + 0.5f ;
 	for (int i = 0; i < (int)this->starPunctuation; i++) {
		graphic.drawTexture(STAR, 0, 0, 69, 61, 560 + i*80, 325, 69, 61);
	}
}

void Menu::renderRanking(SDLInterface& graphic) {
	graphic.drawTexture(BACKGROUND, 0, 0, 1920, 1200, 0, 0, SCREEN_WITDH, SCREEN_HEIGHT);
	graphic.drawTexture(LEADERBOARD, 0, 0, 557, 937, SCREEN_WITDH / 2 - 300 / 2, 50, 357, 737);
	vector<Player> players = FileReader::LoadPlayersBinary("../sharedResources/files/store/ranking.bin");
	std::string player;

	for (int i = 0; i < players.size(); i++) {
		if (i >= 10) {
			break;
		}
		player = std::to_string(i + 1) + "." + players.at(i).getName() + " : " + std::to_string(players.at(i).getScore());
		switch (i) {
		case 0:
			graphic.drawText(player, BLACK, GOLD, SCREEN_WITDH / 2 -sizeof(player), 50 * i + 200);
			break;
		case 1:
			graphic.drawText(player, BLACK, SILVER, SCREEN_WITDH / 2-sizeof(player) , 50 * i + 200);
			break;
		case 2:
			graphic.drawText(player, BLACK, BRONZE, SCREEN_WITDH / 2-sizeof(player) , 50 * i + 200);
			break;
		default:
			graphic.drawText(player, WHITE, SCREEN_WITDH / 2-sizeof(player) , 50 * i + 200);
			break;
		}
	}
	backToMenuButton.draw(graphic);

}

int Menu::onClickButtons(glm::ivec2 d) {
	if (newGame.detectPressed(d.x, d.y)) {
		return BUTTON_NEW_GAME;
	}
	if (loadGame.detectPressed(d.x, d.y)) {
		return BUTTON_LOAD_GAME;
	}
	if (creditsButton.detectPressed(d.x, d.y)) {
		return BUTTON_CREDITS;
	}
	if (backToMenuButton.detectPressed(d.x, d.y)) {
		return BUTTON_BACK_TO_MENU;
	}
	if (exitButton.detectPressed(d.x, d.y)) {
		return BUTTON_EXIT;
	}
	if (ranking.detectPressed(d.x, d.y)) {
		return BUTTON_RANKING;
	}
	return -1;
}