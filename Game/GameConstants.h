#pragma once

//Game general information
#define SCREEN_WITDH 1400
#define SCREEN_HEIGHT 900
#define GAME_SPEED 0.1f
#define GAME_TEXTURES 27
#define NUM_GUNTER 5
#define CELL_WIDTH 70

//Sprite information
#define SPRITE_SPEED 0.01f
#define SPRITE_FIRE_WIDTH 69
#define SPRITE_FIRE_HEIGHT 70
#define SPRITE_HERO 0
#define SPRITE_FIRE 1
#define BACKGROUND 2
#define BUTTON 3
#define OVNI_IMAGE 4
#define LEADERBOARD 25

// Tiles type
#define CLOUD 5
#define PLATFORM_TOP_RIGHT 6
#define PLATFORM_TOP_LEFT 7
#define PLATFORM_BOTTOM_RIGHT 8
#define PLATFORM_BOTTOM_LEFT 9
#define PLATFORM_CENTER_TOP 10
#define PLATFORM_CENTER_BOTTOM 11
#define PLATFORM_CENTER 12
#define PLATFORM_CENTER_LEFT 13
#define PLATFORM_CENTER_RIGHT 14
#define WATER 15
#define WATE_BUBBLES 16
#define NON_MATERIAL 17
#define SIGNAL_RIGHT 18
#define SIGNAL_FINAL 19
#define HELICOPTER 20
#define GUNTER 21
#define LOSE_SCREEN 22
#define WIN_SCREEN 23
#define STAR 24
#define HELICOPTER_LEFT 26


//Color information
#define GAME_BASIC_COLORS 8
#define RED 0
#define GREEN 1
#define BLUE 2
#define BLACK 3
#define WHITE 4
#define GOLD 5
#define SILVER 6 
#define BRONZE 7


// Buttons
#define BUTTON_NEW_GAME 0
#define BUTTON_LOAD_GAME 1
#define BUTTON_EXIT 2
#define BUTTON_CREDITS 3
#define BUTTON_BACK_TO_MENU 4
#define BUTTON_RANKING 5

//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{ INIT, PLAY, EXIT, MENU, CREDITS, WIN, LOSE, RANKING};

// enum class for the type of collision state
enum class CollisionState{DEAD, CAPTURE, FINNISH};