#pragma once
#include "Element.h"

class Person : public Element {
public:
	Person();
	Person(int resource, int x, int y);
	~Person();
};

