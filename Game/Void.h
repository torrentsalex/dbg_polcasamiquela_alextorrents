#pragma once
#include "Element.h"
class Void :
	public Element {
public:
	Void();
	~Void();
	Void(int resource, int x, int y);
};

