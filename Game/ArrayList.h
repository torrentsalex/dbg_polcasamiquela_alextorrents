#pragma once

template <class Data>
class ArrayList {
private:
	//A dynamic array of person's object
	Data * elements;
	//The number of people
	int numElements;

public:
	/**
	* Constructor
	*/
	ArrayList() {
		elements = 0;
		numElements = 0;
	}


	/**
	* Destructor
	*/
	~ArrayList() {
	}

	/**
	* Add a person into the data structure. Thus, it has to ask memory for the new size and copy the previous people
	* @param p is the new person to add
	*/
	void ArrayList::add(Data & p) {
		Data * temp;

		temp = new Data[numElements + 1];
		if (temp == 0) {
		//	throw exception("[GenericList::add] Memory was not able to allocate memory");
		}
		for (int i = 0; i < numElements; i++) {
			temp[i] = elements[i];
		}
		temp[numElements] = p;

		if (numElements != 0) {
			delete[] elements;
		}

		numElements++;

		elements = temp;
	}
	/*
	* Removes an element from the data structure
	*/
	void remove(int id) {
		Data * temp;
 		 
		if (id < numElements) {
			if (numElements == 1) {
				numElements = 0;
				delete[] elements;
			} else {
				Data * temp = new Data[numElements - 1];
				//Copy the elements until the id th position
				for (int i = 0; i < id; i++) {
					temp[i] = elements[i];
				}
				//Skip the element at the id th position
				for (int i = id; i < numElements; i++) {
					temp[i] = elements[i + 1];
				}
+				delete[] elements;
	  			elements = temp;
			}
			numElements--;
		} else {
			//throw exception("[GenericList::remove] The  position doesn't exist");
		}
	}

	/*
	* Gets a person from the data structure
	* @param id is the person to select
	*/
	Data & get(int id) {
		return elements[id];
	}

	/*
	* Gets the size of the data structure
	* @return the size of the array
	*/
	int getSize() {
		return numElements;
	}
};