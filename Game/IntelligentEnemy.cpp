#include "IntelligentEnemy.h"



IntelligentEnemy::IntelligentEnemy() {
}


IntelligentEnemy::~IntelligentEnemy() {
}

IntelligentEnemy::IntelligentEnemy(int resource, int x, int y) : Element(resource, x*70, y*70) {
	this->originX = x * 70;
	this->originY = y * 70;
	eState = EnemyState::INIT;
}

void IntelligentEnemy::update() {
	switch (eState) {
	case EnemyState::INIT:
		if (frameCounter >= 150) {
			eState = EnemyState::SEARCHING;
			frameCounter = 0;
		}
		break;
	case EnemyState::SEARCHING:
		// 60 frames searching for de player if not searched, other 60 frames
		// Go to moving
		break;
	case EnemyState::MOVING:
		// 60 frames moving to player
		// and return to searching 
		break;
	case EnemyState::DEAD:
		// If collisioned, 120 frames in this state, reset and go to searching state
		break;
	}
}