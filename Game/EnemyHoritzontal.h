#pragma once
#include "Element.h"


enum class EnemyHoritzontalState {RIGHT, LEFT};

class EnemyHoritzontal : public Element {
	int originX;
	EnemyHoritzontalState eState;

public:
	EnemyHoritzontal();
	~EnemyHoritzontal();
	EnemyHoritzontal(int resource, int x, int y);

	void update();
};

