 #include "Game.h"
#include "GameConstants.h"

int main(int argc, char ** argv) {
	Game game("ufo rescue", SCREEN_WITDH, SCREEN_HEIGHT);
	try {
		game.run();
	}
	catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
	return 0;
}