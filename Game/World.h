#pragma once
#include "Character.h"
#include "Map.h"
#include "Person.h"
#include "EnemyVertical.h"
#include "EnemyHoritzontal.h"
#include "SDLInterface.h"
#include <sstream>
#include <string>
#include "Collision.h"
#include "Log.h"
#include "LinkedListPointer.h"
#include "FileReader.h"

class World {
private:
	Character pj;
	Map map;
	int screenWidth;
	int screenhHeight;
	LinkedListPointer<Element*> listOfPeople;
	LinkedListPointer<Element*> listOfEnemies;
	//const int SIZE_PEOPLE;

	bool finnished = false;

	// String to draw
	std::stringstream sstm;
	std::string capturedText;

	//
	bool saved = false;
	std::string stringSave = "SAVED!";
	int savedCounter = 0;


public:
	World();
	World(World& world);
	World(int screenWidth, int screenHeight);
	~World();
	void calculateRanking();

	void drawWorld(SDLInterface& graphic);
	void updateWorld(GameState& gameState);
	void physicsWorld();
	void executePlayerCommands(SDLInterface& graphic);

	bool newGame();
	bool loadGame();

	int getFinalPuntuaction();

	void saveCurrentGame();
};

