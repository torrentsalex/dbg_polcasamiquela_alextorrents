#include "EnemyVertical.h"

EnemyVertical::EnemyVertical() {
}


EnemyVertical::~EnemyVertical() {
}

EnemyVertical::EnemyVertical(int resource, int x, int y) {
	this->resource = resource;
	this->x = x * 70;
	this->originY = y * 70;
	this->y = y * 70;
	eState = EnemyVerticalState::UP;
}

// Move the object verticallyy until 3 cells
void EnemyVertical::update() {
	switch (eState) {
	case EnemyVerticalState::UP:
			this->y++;
			if (this->y >= originY + 70 * 3) { // 70 for each cell for 3 cells
				eState = EnemyVerticalState::DOWN;
			}
			break;
	case EnemyVerticalState::DOWN:
		this->y--;
		if (this->y <= originY) {
			eState = EnemyVerticalState::UP;
		}
		break;
	}
}