#pragma once

//Third-party libraries
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <iostream>
#include <time.h>
#include "GameConstants.h"
#include "SDLInterface.h"
#include "InputManager.h"
#include "Sprite.h"
#include "Menu.h"
#include "Character.h"
#include "World.h"
#include "Button.h"
/*
* The Game class manages the game execution
*/
enum class Error_Load{none, new_game, load_game, no_ranking};

class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight);	//Constructor
		~Game();															//Destructor
		void run();															//Game execution	

	private:
			//Attributes	
		std::string _windowTitle;		//SDLInterface Title
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		GameState _gameState;			//It describes the game state				
		SDLInterface _graphic;			//Manage the SDL graphic library		
		int _lastTimeMonsterWasUpdated;				//Counter to compute the time
		Menu myMenu;
		World world;
		Button exitButton;
		
		// variables for load/new game 
		Error_Load errorLoad = Error_Load::none;
		bool errorLoadBool = false;
		int errorLoadCounter = 0;
			
		//Internal methods for the game execution
		void init();
		void gameLoop();		
		void executePlayerCommands();
		void doPhysics();
		void renderGame();
		void drawSprite(Sprite & e);		
};

