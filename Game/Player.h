#pragma once
#include <string>

#define max_char 50

class Player {
	
	char name[max_char];
	//std::string name;
	int score;

public:
	Player();
	Player(const Player & p);
	Player(char name[], int score);
	~Player();
	
	void setName(char name[]);
	void setScore(int score);

	char* getName();
	int getScore();

	bool operator<(const Player& val) const {
		return score > val.score;
	}
};

