#include "Element.h"



Element::Element() {
	this->resource = NON_MATERIAL;
}

Element::~Element() {
}

Element::Element(Element& e) {
	this->resource = e.resource;
	// we multiply the x and y with the width and height
	this->x = e.x;
	this->y = e.y;
}

Element::Element(int resource, int x, int y) {
	this->resource = resource;
	// we multiply the x and y with the width and height
	this->x = x * 70;
	this->y = y * 70;
}

void Element::draw(SDLInterface& graphic) {
	graphic.drawTexture(resource, 0, 0, WIDTH, HEIGHT, this->x, this->y, WIDTH, HEIGHT); 
}

void Element::update() {
}

int Element::getTexture() {
	return -1;
}

int Element::getX() {
	return x;
}

int Element::getY() {
	return y;
}

int Element::getWidth() {
	return WIDTH;
}

int Element::getHeight() {
	return HEIGHT;
}