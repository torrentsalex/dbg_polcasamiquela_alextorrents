#pragma once

template<class DataType> class NodePointer;
template<class DataType> class LinkedListPointer;


template <class DataType>
class NodePointer {
	//We are giving "permissions" to the LinkedListPointer class to access to the private members of the NodePointer class. 
	//Thus, the Linked list methods are able to access to data and next attributes directly
	//In this case it has sense give LinkedListPointer "full" permissions because the NodePointer class has only sense for the LinkedListPointer class
	friend class LinkedListPointer<DataType>;
	DataType data;
	NodePointer<DataType> * next;
public:

	NodePointer(DataType p) {
		this->data = p;
		this->next = 0;
	}

	NodePointer(const NodePointer & n) {
		this->data = n.data;
		this->next = n.next;
	}

	~NodePointer() {
		delete data;
	}
};


/*
* This class represents a linked list, which is a linear data structure characterized by its capabilities of adding and removing information in a fast way
*/
template <class DataType>
class LinkedListPointer {
	NodePointer<DataType> * head;
	int numElements;
public:
	/*
	* Creates an empty LinkedListPointer
	* @param type is the type of the template. 0 means that is a pointer. Otherwise it is equal to 1
	*/
	LinkedListPointer() {
		head = 0;
		numElements = 0;
	}

	/*
	* Destructor. It goes through the linked list while it deletes each one of the nodes
	*/
	~LinkedListPointer() {
		//Destroy a LinkedListPointer means removing all its elements
		for (int i = 0; i < numElements; i++) {
			removeAll();
		}
	}

	/*
	* Add a person at the beginning of the LinkedListPointer. It throws an exception if system is not able to allocate memory
	* @param p is the person object to add
	*/
	void add(DataType p) {
		NodePointer<DataType> * n;
		//Create a new node that will contain the object to add
		n = new NodePointer<DataType>(p);
		if (n == 0) {
			//throw exception("System is not able to allocate memory for the new node");
		}

		//The new node will point to the first element pointed by the head pointer through its n->next pointer. It head pointed to null, the next node of n will point to null which is ok
		n->next = head;
		//Finally, the head pointer points to the new element
		head = n;
		numElements++;
	}

	/*
	* Remove the first person from the LinkedListPointer
	* @throw an exception if the LinkedListPointer was empty
	*/
	void remove(int id) {
		NodePointer<DataType> * currentElement;
		NodePointer<DataType> * tmpElement;

		if (numElements == 0) {
			//throw exception("System cannot delete the first position because the LinkedListPointer is empty");
		}

		if (id == 0) {
			tmpElement = head;
			head = head->next;
		}
		else {
			currentElement = head;
			for (int i = 0; i < id - 1; i++) {
				currentElement = currentElement->next;
			}
			tmpElement = currentElement->next;
			currentElement->next = tmpElement->next;
		}
		delete tmpElement;
		numElements--;
	}
	
	void removeAll() {
		NodePointer<DataType> * currentElement;

		if (numElements == 0) {
			return;
			//	throw exception("System cannot delete the first position because the LinkedListPointer is empty");
		}
		//Head will point to the next element. It doesn't matter if there was only one element, head will point to null and numElements will be equal to 0. Thus, everything will be ok
		currentElement = head;
		head = currentElement->next;

		delete currentElement;
		numElements--;
	}
	/*
	* Get a specified Character from the LinkedListPointer
	* @param position is where the new object is stored
	* @return person is the specified person to return
	* @throw an exception if the specified position doesn't exist
	*/
	DataType& get(int position) {
		if (position >= numElements) {
			//throw exception("The specified position doesn't exist");
		}

		NodePointer<DataType> * currentElement;
		//We go through the data structure counting the elements-1 because we want to stop the search in previous element 
		currentElement = head;
		for (int i = 0; i <= position - 1; i++) {
			currentElement = currentElement->next;
		}
		return currentElement->data;
	}
	
	NodePointer<DataType> * getPointer(int position) {

		NodePointer<DataType> * currentElement;
		//We go through the data structure counting the elements-1 because we want to stop the search in previous element 
		currentElement = head;
		for (int i = 0; i <= position - 1; i++) {
			currentElement = currentElement->next;
		}

		return currentElement;
	}

	int getSize() {
		return numElements;
	}
};