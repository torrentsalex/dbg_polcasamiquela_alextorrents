#pragma once
#include "Element.h"

class Start : public Element {
public:
	Start();
	Start(int type, int x, int y);
	~Start();
	int getTexture();
};

