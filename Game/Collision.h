#pragma once
#include "Element.h"
#include "Character.h"

class Collision {
private:
	Collision();
public:
	~Collision();

	static bool GetColision(Character& c, Element& e);
};

