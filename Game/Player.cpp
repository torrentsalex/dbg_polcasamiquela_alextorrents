#include "Player.h"

Player::Player() {
}

Player::~Player() {
}

Player::Player(const Player& p) {
	score = p.score;
	strcpy(name, p.name); // copy p.name into name
}

Player::Player(char iName[], int iScore) {
	strcpy(name, iName);
	score = iScore;
}

// Setters
void Player::setName(char iName[]) {
	strcpy(name, iName);
}

void Player::setScore(int iScore) {
	score = iScore;
}

// Getters
char* Player::getName() {
	return name;
}

int Player::getScore() {
	return score;
}