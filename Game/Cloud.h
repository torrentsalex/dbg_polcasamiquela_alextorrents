#pragma once
#include "Element.h"
#include "GameConstants.h"
#include <string>

#define WIDTH_CLOUD 140
#define HEIGHT_CLOUD 70


class Cloud : public Element {
private:

public:
	Cloud();
	Cloud(int type, int x, int y);
	~Cloud();

	void update();
	void draw(SDLInterface& graphic);
};

