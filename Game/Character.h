#pragma once
#include "SDLInterface.h"
#include "Sprite.h"
#include "GameConstants.h"
#include "Element.h"
#include "Player.h"

// States for movement horizontal and vertical 
enum class MoveHorizontal {RIGHT, LEFT, UNKNOW};
enum class MoveVertical {DOWN, UP};

// Pixels movement per state
#define MOVEMENT 7

class Character : public Element, public Sprite {
private: 
	int width, imageWidth;
	int height, imageHeight;
	float gravity = .1f;
	float antiGravity = -1.5f;
	float velocity = 1;
	float hvelocity = 1;
	MoveHorizontal horizontalMove;
	MoveVertical verticalMove;
	Player player;
	bool cDead = false;

	/* restart all velocities, is privatet because we only call from its class*/
	void restartVelocities();

	void drawSprite(SDLInterface& graphic);

public:
	Character();
	Character(const Character& c);
	Character(int texture, float x, float y, char name[]);
	~Character();

	void drawCharacter(SDLInterface& graphic);
	void update();
	void upPlayer();

	void setDeadProperties(bool dead);
	void setPunctuation(int punctuation);
	void setPlayer(Player p);

	void goRight();
	void goLeft();

	int getX();
	int getY();
	int getWidth();
	int getHeight();
	void collisioned();
	void plusPoints();
	int getPoints();
	std::string getName();
	Player getPlayer();
};

