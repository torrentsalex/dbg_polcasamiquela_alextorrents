#pragma once
#include "SDLInterface.h"

#define WIDTH 70
#define HEIGHT 70

class Element {
protected:
	int x;
	int y;
	int resource;

public:
	Element();
	Element(Element& e);
	Element(int type, int x, int y);
	~Element();

	virtual int getTexture();
	virtual void update();
	virtual void draw(SDLInterface& graphic);

	int getX();
	int getY();
	int getWidth();
	int getHeight();
};