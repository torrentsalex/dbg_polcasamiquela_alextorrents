#pragma once
#include <string>
#include <iostream>

class Log {
private:
	static const bool debug = false;
public:
	Log();
	~Log();
	static void print(std::string string);
	static void print(std::string string, int i);
};

