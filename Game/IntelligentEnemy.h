#pragma once
#include "Element.h"

enum class EnemyState {INIT, SEARCHING, MOVING, DEAD};

class IntelligentEnemy :
	public Element {
private:
	int originX;
	int originY;
	int frameCounter;
	EnemyState eState;
public:
	IntelligentEnemy();
	~IntelligentEnemy();

	IntelligentEnemy(int resource, int x, int y);

	void update();
};

