#pragma once
#include "Element.h"
#include <string>

#define WIDTH_WALL 70
#define HEIGHT_WALL 70

class Wall : public Element {
private:

public:
	Wall();
	Wall(int type, int x, int y);
	~Wall();
	int getTexture();
};

