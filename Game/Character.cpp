#include "Character.h"

Character::Character() {
}

Character::~Character() {
}

Character::Character(int cTexture, float cX, float cY, char name[]):
	player(name, 0) {

	resource = cTexture;
	x = cX;
	y = cY;
	this->imageWidth = 588;
	this->imageHeight = 586;
	this->width = 70;
	this->height = 70;
	this->verticalMove = MoveVertical::DOWN;
	this->horizontalMove = MoveHorizontal::UNKNOW;
	// set the sprite texture for dead 
	setInitialValues(x, y, SPRITE_FIRE, 0, 25);
}

Character::Character(const Character& c) {

	this->x = c.x;
	this->y = c.y;

	this->player = c.player;
	
	this->imageWidth = 588;
	this->imageHeight = 586;
	this->width = 70;
	this->height = 70;
	this->verticalMove = MoveVertical::DOWN;
	this->horizontalMove = MoveHorizontal::UNKNOW;
	// set the sprite texture for dead 
	setInitialValues(x, y, SPRITE_FIRE, 0, 25);
}


void Character::drawCharacter(SDLInterface& graphic) {
	// this line have to be in update, but we need the graphic object so... for the moment, i guess, i coded here
	if (cDead) {
		drawSprite(graphic);
		nextFrame((int)(graphic.getCurrentTicks() * SPRITE_SPEED));
	} else {
		graphic.drawTexture(OVNI_IMAGE, 0, 0, this->imageWidth, this->imageHeight, (int)this->x, (int)this->y, this->imageWidth / 8, this->imageHeight / 8);
	}
}


void Character::update() {

	if (cDead) {
		return;
	}

	switch (verticalMove) {
		case MoveVertical::DOWN:
			// Collision with screen
			if (this->y >= SCREEN_HEIGHT-80) {
				this->y = SCREEN_HEIGHT - 80;
			}
			velocity += gravity;
			this->y += velocity;
			break;
		case MoveVertical::UP:
			// Collision with screen
			if (this->y <= 0) {
				this->y = 0;
			}
			velocity += antiGravity;
			this->y += velocity;
			if (velocity <= -MOVEMENT) {
				velocity = 1;
				this->verticalMove = MoveVertical::DOWN;
			}
			break;
	}
	switch(horizontalMove) {
		case MoveHorizontal::LEFT:
			// Collision with screen
			if (this->x <= 0) {
				this->x = 0;
			}
			hvelocity += antiGravity;
			this->x += hvelocity;
			if (hvelocity <= -MOVEMENT) {
				hvelocity = 1;
				this->horizontalMove = MoveHorizontal::UNKNOW;
			}
			break;
		case MoveHorizontal::RIGHT:
			// Collision with screen
			if (this->x >= SCREEN_WITDH - 80) {
				this->x = SCREEN_WITDH - 80;
			}
			hvelocity += antiGravity;
			this->x -= hvelocity;
			if (hvelocity <= -MOVEMENT) {
				hvelocity = 1;
				this->horizontalMove = MoveHorizontal::UNKNOW;
			}
			break;
		case MoveHorizontal::UNKNOW:
				break;
	}
}

void Character::setDeadProperties(bool dead) {
	cDead = dead;
}

void Character::setPunctuation(int punctuation) {
	player.setScore(punctuation);
}

void Character::setPlayer(Player p) {
	player.setName(p.getName());
	player.setScore(p.getScore());
}

// Characters Movements
void Character::upPlayer() {
	this->verticalMove = MoveVertical::UP;
}

void Character::goRight() {
	this->horizontalMove = MoveHorizontal::RIGHT;
}

void Character::goLeft() {
	this->horizontalMove = MoveHorizontal::LEFT;
}

void Character::restartVelocities() {

}

void Character::plusPoints() {
	int points = player.getScore() + 10;
	player.setScore(points);
}

// Getters
int Character::getX() {
	return x;
}

int Character::getY() {
	return y;
}

int Character::getWidth() {
	return this->width;
}

int Character::getHeight() {
	return this->height;
}

int Character::getPoints() {
	return player.getScore();
}

std::string Character::getName() {
	return player.getName();
}

Player Character::getPlayer() {
	return player;
}

// Collisions
void Character::collisioned() {
	velocity = 0;
	y -= 1;
}

void Character::drawSprite(SDLInterface& graphic) {
	graphic.drawTexture(SPRITE_FIRE, SPRITE_FIRE_WIDTH* getCurrentFrame(), 0, SPRITE_FIRE_WIDTH, SPRITE_FIRE_HEIGHT
		, x, y, SPRITE_FIRE_WIDTH, SPRITE_FIRE_HEIGHT);
}