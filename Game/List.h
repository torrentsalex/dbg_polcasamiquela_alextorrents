#pragma once
#include <vector>
/*
* This class represents an array of people.
* This implementation is based on allocation the space only when is needed.
* It is an optimal solution from the point of view of the memory usage because it only uses the memory that is needed in each moment.
* It is totally inefficient from the computational point of view due because it has to swap all the content when a new person is added or a person is deleted
*/

template <class Data>

class List {
protected:
	//A dynamic array of person's object
	std::vector <Data *> _elements;
	//The number of people
	int _iterator;

public:
	//Constructor
	List() {

	}
	//Destructor
	~List() {

	}
	//Add element
	void add(Data  p) {
		_elements.push_back(p);
	}

	void addElement(Data & p) {
		Data * temp;

		temp = new Data[_numElements + 1];
		if (temp == 0) {
			//std::cout << "[ListOfPeople::addPerson] System was not able to allocate memory";
			exit(-1);
		}
		for (int i = 0; i < _numElements; i++) {
			temp[i] = _elements[i];
		}
		temp[_numElements] = p;

		if (_numElements != 0) {
			delete[] _elements;
		}

		_numElements++;

		_elements = temp;
	}

	//Delete element
	void del() {
		_elements.pop_back();
	}

	void remove(int position) {
		_elements._Pop_back_n(position);
	}

	//Print the content
	void printListofObjects(SDLInterface & graphic) {

		begin();
		while (!end()) {

			Data * element = nextElement();
			element->drawSprite(element->getSprite(), graphic);
		}

	}

	Data get(int position) {
		begin();
		while (_iterator < position - 1) {
			nextElement();
		}
		return nextElement();
	}
	
	int getSize() {
		return _elements.size();
	}

	void begin() {
		_iterator = 0;
	}

	Data nextElement() {
		_iterator++;
		return _elements[_iterator - 1];
	}

	bool end() {
		if (_iterator == _elements.size()) return true;
		return false;
	}
};
