#include "Map.h"

Map::Map() {
	// Load the map with the file
	vector<vector<int>> vClouds = FileReader::LoadArrayFromFile("../sharedResources/files/clouds.txt");
	vector<vector<int>> vPlatforms = FileReader::LoadArrayFromFile("../sharedResources/files/platforms.txt");
	for (int i = 0; i < 20; i++) {		
		for (int j = 0;j < 13;j++) {
			// INIT BACKGROUND
			map[i][j][DIMENSION_BACKGROUND] = new Cloud(vClouds.at(j).at(i), i, j);
			// INIT PLATFORMS
			switch (vPlatforms.at(j).at(i)) {
				case SIGNAL_FINAL:
					map[i][j][DIMENSION_PLATFORMS] = new Final(vPlatforms.at(j).at(i), i, j);
				case SIGNAL_RIGHT:
					map[i][j][DIMENSION_PLATFORMS] = new Start(vPlatforms.at(j).at(i), i, j);
					break;
				case NON_MATERIAL:
					map[i][j][DIMENSION_PLATFORMS] = new Void(vPlatforms.at(j).at(i), i, j);
				default: 
					map[i][j][DIMENSION_PLATFORMS] = new Wall(vPlatforms.at(j).at(i), i, j);
					break;
			}
		}
	}
}

// dame
Map::~Map() {

}


void Map::drawMap(SDLInterface& graphic) {
	for (int i = 0; i < 20; i++) {
		for (int j = 0;j < 13;j++) {
			map[i][j][DIMENSION_BACKGROUND]->draw(graphic);
		}
	}
	for (int i = 0; i < 20; i++) {
		for (int j = 0;j < 13;j++) {
			map[i][j][DIMENSION_PLATFORMS]->draw(graphic);
		}
	}
}

void Map::update() {
	for (int i = 0; i < 20; i++) {
		for (int j = 0;j < 13;j++) {
			map[i][j][DIMENSION_BACKGROUND]->update();
		}
	}
}

int Map::getTexture(int x, int y) {
	int positionx = x / 70;
	int positiony = y / 70;
	return map[positionx][positiony][DIMENSION_PLATFORMS]->getTexture();
}

Element& Map::getCell(int x, int y) {
	int positionx = x / 70;
	int positiony = y / 70;
	return *map[positionx][positiony][DIMENSION_PLATFORMS];
}
