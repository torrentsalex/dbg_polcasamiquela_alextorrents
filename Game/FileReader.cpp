#include "FileReader.h"

FileReader::FileReader() {
}


FileReader::~FileReader() {
}

vector<vector<int>> FileReader::LoadArrayFromFile(string fileName) {
	string line;
	int rows = getRowSize(fileName);
	int columns = getColumnSize(fileName);

	ifstream myfile(fileName);
	vector<vector<int>> myArray;

	if (myfile.is_open()) {
		vector<int> vline;
		while (getline(myfile, line)) {
			vline = split(line);
			myArray.push_back(vline);
		}
		myfile.close();
	} else {
		cout << "Unable to open file!" << endl;
	}
	return myArray;
}


vector<Player> FileReader::LoadPlayersBinary(std::string fileName) {
	std::ifstream myInputFile;
	std::streampos fileSize;
	Player p;
	int sizePerson = sizeof(Player);

	//Read the file with the random population
	myInputFile.open(fileName, std::ios::binary);
	if (!myInputFile.is_open()) {
		throw std::exception("LoadPlayersBinary] System was not able to open the file");
	}
	//Compute the filesize
	myInputFile.seekg(0, std::ios::end);
	fileSize = myInputFile.tellg();
	myInputFile.seekg(0, std::ios::beg);

	//Read the content
	int numElements = fileSize / sizePerson;
	vector<Player> players;
	for (int i = 0; i < numElements; i++) {
		myInputFile.read((char *)(&p), sizePerson);
		players.push_back(p);
	}

	myInputFile.close();
	return players;
}

void FileReader::SaveArrayToFile(vector<vector<int>> vector, std::string fileName) {
	ofstream file(fileName);
	if (file.is_open()) {
		for (int i = 0; i < vector.size(); i++) {
			for (int j = 0; j < vector.at(i).size(); j++) {
				file << vector.at(i).at(j);
				// Add ; all lines less the last line 
				if (j < vector.at(i).size()-1) {
					file << ";";
				}
			}
			file << "\n";
		}
		file.close();
	} else {
		cout << "unable to open file: " << fileName << endl;
	}
}
void FileReader::SavePlayerBinary(vector<Player> player, std::string fileName, bool endFile) {
	ofstream file;
	if (endFile) {
		// Append the vector at the end of the file
		file = ofstream(fileName, ios::app | ios::binary);
	} else {
		// Truncate the file
		file = ofstream(fileName, ios::trunc | ios::binary);
	}

	if (file.is_open()) {
		int sizePlayer;
		for (Player p : player) {
			sizePlayer = sizeof(p);
			file.write((char*)(&p), sizePlayer);
		}
		file.close();
	} else {
		cout << "unable to open file: " << fileName << endl;
	}
}


bool FileReader::CheckExistFile(std::string fileName) {
	ifstream f(fileName);
	if (f.good()) {
		f.close();
		return true;
	} else {
		f.close();
		return false;
	}
}

// Utils
/*
*	Split the line with the delimiter ';' and put into a vector 
*/
vector<int> FileReader::split(const string &line) {
	char delim = ';';
	stringstream ssLine(line);
	string item;
	vector<int> tokens;
	while (getline(ssLine, item, delim)) {
		tokens.push_back(atoi(item.c_str())); // Convert the string to a int 
	}
	return tokens;
}

/*
*	open file and return the number of lines of the file	
*/
int FileReader::getRowSize(string file) {
	ifstream myfile(file);
	string line;
	int numberLines = 0;
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			numberLines++;
		}
		myfile.close();
	} else {
		cout << "Unable to open file!" << endl;
	}
	return numberLines;
}

/*
* 
*/
int FileReader::getColumnSize(string file) {
	ifstream myfile(file);
	string line;
	int numberColumns = 0;
	if (myfile.is_open()) {
		getline(myfile, line);
		vector<int> lineSplitted = split(line);
		numberColumns = lineSplitted.size();
		myfile.close();
	} else {
		cout << "Unable to open file!" << endl;
	}
	return numberColumns;
}


ifstream FileReader::openFile(string file) {
	ifstream myfile(file);
	string line;
	int numberColumns = 0;
	if (!myfile.is_open()) {
		cout << "Unable to open file!" << endl;
	}
	return myfile;
}