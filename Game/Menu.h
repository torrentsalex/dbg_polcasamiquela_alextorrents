#pragma once
#include "Button.h"
#include "SDLInterface.h"
#include "Player.h"
#include "FileReader.h"

class Menu {
	int screenWidth;
	int screenHeight;

	Button newGame;
	Button loadGame;
	Button creditsButton;
	Button backToMenuButton;
	Button exitButton;
	Button ranking;

public:
	Menu();
	Menu(int screenWidth, int screenHeight);
	~Menu();

	void renderMenu(SDLInterface& graphic);
	void renderCredits(SDLInterface& graphic);
	void renderLose(SDLInterface& graphic, int punctuation);
	void renderWin(SDLInterface& graphic, int punctuation);
	void renderRanking(SDLInterface& graphic);
	/**
		Return the id of the button clicked, -1 if any button have clicked
	*/
	int onClickButtons(glm::ivec2 d);

	float starPunctuation;
};

